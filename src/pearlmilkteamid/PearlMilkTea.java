/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pearlmilkteamid;

/**
 *
 * @author tud08
 */
public class PearlMilkTea extends OrderPMT {//Class PearlMilkTea สืบทอดคลาสจาก OrderPMT 

    public PearlMilkTea(String name, int numOfGlass, int syrub, String size) {//method PearlMilkTea เรียกใช้ ชื่อ, จำนวนแก้ว, จำนวนช้อนน้ำเชื่อม, ขนาดไซส์
        super(name, numOfGlass, syrub, size);                                                               //เรียกใช้ ชื่อ, จำนวนแก้ว, จำนวนช้อนน้ำเชื่อม, ขนาดไซส์ จาก class OrderPMT
    }

    public PearlMilkTea(String name, int numOfGlass, int syrub) {//method PearlMilkTea เรียกใช้ ชื่อ, จำนวนแก้ว, จำนวนช้อนน้ำเชื่อม
        super(name, numOfGlass, syrub);                                                     //เรียกใช้ ชื่อ, จำนวนแก้ว, จำนวนช้อนน้ำเชื่อม จาก class OrderPMT
    }

    public PearlMilkTea(String name, int numOfGlass) {//method PearlMilkTea เรียกใช้ ชื่อ,จำนวนแก้ว
        super(name, numOfGlass);                                                //เรียกใช้ ชื่อ, จำนวนแก้ว จาก class OrderPMT
    }

    public PearlMilkTea(String name) {//method PearlMilkTea เรียกใช้ ชื่อ
        super(name);                                            //เรียกใช้ ชื่อ, จำนวนแก้ว จาก class OrderPMT
    }

    @Override
    public void calOrder() {                        //method calOrder แสดงการคำนวน return void
        if (numOfGlass < 6) {                               //ถ้าจำนวนแก้วน้อยกว่า 6
            if (numOfGlass == 1) {                              //ถ้าจำนวนแก้วเท่ากับ 1
                if (syrub == 0) {                                           //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {      //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {      //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวาน
                } else {                                                          //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");    //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 2) {                                //ถ้าจำนวนแก้วเท่ากับ 2
                if (syrub == 0) {                                            //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");      //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {       //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {       //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                           //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 3) {                                 //ถ้าจำนวนแก้วเท่ากับ 3
                if (syrub == 0) {                                             //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                             //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 4) {                                  //ถ้าจำนวนแก้วเท่ากับ 4
                if (syrub == 0) {                                              //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0      
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");  //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");              //แสดงว่าชอบหวานมาก
                } else {                                                             //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 5) {                                  //ถ้าจำนวนแก้วเท่ากับ 5
                if (syrub == 0) {                                              //ถ้า input จำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                            //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose Pearl Milk Tea number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
        } else {                                                      //ถ้าจำนวนแก้วมากกว่า 6
            System.out.println("You over order!!!");           //แสดงข้อความบอกว่าเกินจำนวน
        }
        GlassSize();
        System.out.println(" ");
    }
}