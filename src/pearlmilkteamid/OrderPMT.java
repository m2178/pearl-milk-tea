/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pearlmilkteamid;

/**
 *
 * @author tud08
 */
public class OrderPMT {//Order Pear Milk Tea

    protected int numOfGlass;               //สร้างตัวแปรเก็บจำนวนแก้วที่สั่ง
    protected int syrub;                          //สร้างตัวแปรเก็บจำนวนช้อนน้ำเชื่อม
    protected String size = "s";              //สร้างตัวแปรเก็บขนาดไซส์ และกำหนดไซส์เริ่มต้นที่ขนาด s 
    protected String name;                     //สร้างตัวแปรเก็บชื่อ
    //Overload method

    public OrderPMT(String name, int numOfGlass, int syrub, String size) {//method OrderPMT เก็บชื่อ จำนวนแก้ว จำนวนช้อนน้ำเชื่อม และขนาดไซส์
        this.numOfGlass = numOfGlass;                   //เก็บตัวแปรจำนวนแก้ว
        this.syrub = syrub;                                         //เก็บตัวแปรจำนวนช้อนน้ำเชื่อม
        this.size = size;                                               //เก็บตัวแปรขนาดไซส์
        this.name = name;                                         //เก็บตัวแปรชื่อ
    }
    //Overload method

    public OrderPMT(String name, int numOfGlass, int syrub) {//method OrderPMT เก็บชื่อ จำนวนแก้ว และจำนวนช้อนน้ำเชื่อม
        this.numOfGlass = numOfGlass;                   //เก็บตัวแปรจำนวนแก้ว
        this.syrub = syrub;                                         //เก็บตัวแปรจำนวนช้อนน้ำเชื่อม
        this.size = "s";                                                //เก็บตัวแปรขนาดไซส์ ตั้งต้นที่ขนาด s
        this.name = name;                                         //เก็บตัวแปรชื่อ
    }
    //Overload method

    public OrderPMT(String name, int numOfGlass) {//method OrderPMT เก็บชื่อ และจำนวนแก้ว
        this.numOfGlass = numOfGlass;                   //เก็บตัวแปรจำนวนแก้ว
        this.syrub = 0;                                                //เก็บตัวแปรจำนวนช้อนน้ำเชื่อม ตั้งต้นที่ 0 ช้อน
        this.size = "s";                                                //เก็บตัวแปรขนาดไซส์ ตั้งต้นที่ขนาด s
        this.name = name;                                         //เก็บตัวแปรชื่อ
    }
    //Overload method

    public OrderPMT(String name) {//method OrderPMT เก็บชื่อ
        this.numOfGlass = 1;                                     //เก็บตัวแปรจำนวนแก้ว เริ่มต้นที่ 1 แก้ว
        this.syrub = 0;                                                //เก็บตัวแปรจำนวนช้อนน้ำเชื่อม ตั้งต้นที่ 0 ช้อน
        this.size = "s";                                                //เก็บตัวแปรขนาดไซส์ ตั้งต้นที่ขนาด s
        this.name = name;                                         //เก็บตัวแปรชื่อ
    }

    public void calOrder() {                        //method calOrder แสดงการคำนวน return void
        if (numOfGlass < 6) {                               //ถ้าจำนวนแก้วน้อยกว่า 6
            if (numOfGlass == 1) {                              //ถ้าจำนวนแก้วเท่ากับ 1
                if (syrub == 0) {                                           //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + "choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {      //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {      //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวาน
                } else {                                                          //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");    //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 2) {                                //ถ้าจำนวนแก้วเท่ากับ 2
                if (syrub == 0) {                                            //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");      //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {       //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {       //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                           //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 3) {                                 //ถ้าจำนวนแก้วเท่ากับ 3
                if (syrub == 0) {                                             //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                             //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 4) {                                  //ถ้าจำนวนแก้วเท่ากับ 4
                if (syrub == 0) {                                              //ถ้าจำนวนช้อนน้ำเชื่อมเท่ากับ 0      
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");  //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");              //แสดงว่าชอบหวานมาก
                } else {                                                             //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
            if (numOfGlass == 5) {                                  //ถ้าจำนวนแก้วเท่ากับ 5
                if (syrub == 0) {                                              //ถ้า input จำนวนช้อนน้ำเชื่อมเท่ากับ 0
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You don't like sweet");     //แสดงว่าไม่ชอบหวาน
                } else if (syrub >= 1 && syrub <= 3) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 1 และน้อยกว่าเท่ากับ 3
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like a little sweet");   //แสดงว่าชอบหวานน้อย
                } else if (syrub >= 4 && syrub <= 6) {         //ถ้าจำนวนช้อนน้ำเชื่อมมากกว่าเท่ากับ 4 และน้อยกว่าเท่ากับ 6
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like sweet");               //แสดงว่าชอบหวานมาก
                } else {                                                            //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                    System.out.println(this.name + " choose number: " + this.numOfGlass + " Choose a syrup: " + this.syrub + " teaspoon");//แสดงชื่อ+จำนวนแก้ว+จำนวนช้อนน้ำเชื่อม
                    System.out.println("You like it very sweet");   //แสดงว่าชอบหวานมากๆ
                }
            }
        } else {                                                      //ถ้าจำนวนแก้วมากกว่า 6
            System.out.println("You over order!!!");           //แสดงข้อความบอกว่าเกินจำนวน
        }
        GlassSize();

    }

    public void GlassSize() {                              //method GlassSize แสดงการคำนวนหาไซส์แก้วที่เลือก return void
        if (numOfGlass < 6) {                                       //ถ้าจำนวนแก้วน้อยกว่า 6        
            if (size == "s" || size == "S") {                           //ถ้าแก้วมีขนาดไซส์ s    
                System.out.println(this.name + " choose the size: " + this.size);//แสดงชื่อ+ขนาดไซส์ที่เลือก
                System.out.println("You choose a small size mug.");         //แสดงข้อความขนาดไซส์เล็ก
            } else if (size == "m" || size == "M") {              //ถ้าแก้วมีขนาดไซส์ m
                System.out.println(this.name + " choose the size: " + this.size);//แสดงชื่อ+ขนาดไซส์ที่เลือก
                System.out.println("You choose a regular size glass.");    //แสดงข้อความขนาดไซส์กลาง
            } else if (size == "l" || size == "L") {                 //ถ้าแก้วมีขนาดไซส์ l
                System.out.println(this.name + " choose the size: " + this.size);//แสดงชื่อ+ขนาดไซส์ที่เลือก
                System.out.println("You choose a large size glass");         //แสดงข้อความขนาดไซส์ใหญ่
            } else if (size == "xl" || size == "XL") {             //ถ้าแก้วมีขนาดไซส์ xl
                System.out.println(this.name + " choose the size: " + this.size);//แสดงชื่อ+ขนาดไซส์ที่เลือก
                System.out.println("You choose the largest size glass."); //แสดงข้อความขนาดไซส์ใหญ่ที่สุด
            } else {                                                                    //ถ้าหากนอกเหนือกว่าที่กล่าวมา
                System.out.println(this.name + " choose the size: " + this.size);//แสดงชื่อ+ขนาดไซส์ที่เลือก
                System.out.println("There is no size you selected!!!");     //แสดงข้อความว่าไม่มีไซส์ที่คุณเลือก
            }
        } else {                                                                //ถ้าจำนวนแก้วมากกว่า 6
            System.out.println("You over order!!!");               //แสดงข้อความว่าไม่มีไซส์ที่คุณเลือก
        }
        System.out.println(" ");
    }
}